import streamlit as st
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import plotly.figure_factory as p_ff
import time
import seaborn as sns

# set the style for seaborn
sns.set_style('darkgrid')
# Suppression des messages de deprecation
st.set_option('deprecation.showfileUploaderEncoding', False)


# Title of the dashboard
st.write("Version de STREAMLIT: " + st.__version__)
st.title('Dashboard Scoring Credit')

# Add a sidebar
st.sidebar.subheader("Choix des fichiers")
uploaded_file = st.sidebar.file_uploader(
                        label="Charger votre fichier CSV ou Excel (200~MB max)",
                         type=['csv', 'xlsx'])

@st.cache
def load_data(f_csv, nrows=None):
    df = pd.read_csv(f_csv, nrows=nrows)
    numeric_cols = df.select_dtypes(['float','int']).columns
    text_cols = df.select_dtypes(['object']).columns
    return df, numeric_cols, text_cols

# Chargement des données
data_load_state = st.text('Chargement des données...')
data, numeric_cols, text_cols = load_data('./resultats/sample_submission_100000.csv')

x = st.sidebar.slider("Nombre de clients recherchés: ", min_value=100,   
                       max_value=data.shape[0], value=data.shape[0]//10, step=data.shape[0]//100)
#add_slider = st.sidebar.slider('Nombre de données',0.0, 10000.0, 2000)
data2, n2_cols, t2_cols = load_data('./resultats/feature_importance_model_A.csv', nrows=x)
data_load_state.text("Done! (... avec du cache)")


# checkbox widget
checkbox = st.sidebar.checkbox(label="Montrer les données")
if checkbox:
    # st.write(data)
    st.dataframe(data=data)
    # create scatterplots
    #st.sidebar.subheader("Scatter plots")
    # add select widget
    #select_box1 = st.sidebar.selectbox(label='X axis', options=numeric_cols)
    #select_box2 = st.sidebar.selectbox(label="Y axis", options=numeric_cols)
    #fig1 = sns.relplot(x=select_box1, y=select_box2, data=data)
    #st.pyplot(fig1)
    df = pd.DataFrame(data[:x], columns=['TARGET'])
    fig, ax = plt.subplots(figsize=(5,2))
    ax.hist(df['TARGET'], bins=150)
    plt.show()
    st.pyplot(fig)


st.sidebar.subheader("Choix du client")
#client = st.sidebar.multiselect(label='Choix du client', options=data['SK_ID_CURR'], default=None)
client = st.sidebar.selectbox(label='Choix du client', options=data['SK_ID_CURR'], index=0)
st.write(f'Vous êtes le client {client}')
df_client = data[data['SK_ID_CURR'] == client]
st.write(df_client)
client_prob = df_client['TARGET']
st.write(f'Votre probailité de remboursement est {client_prob}')


# create histograms
st.sidebar.subheader("Histogram plots")
select_box3 = st.sidebar.selectbox(label="Variables", options=numeric_cols)
slider_histo = st.sidebar.slider(label="Nombre de Bins",min_value=20, max_value=1000, value=100)
st.write(select_box3)
fig2 = sns.distplot(data[select_box3], bins=slider_histo)
st.pyplot(fig2)


# Selection des variables
feature_selection = st.sidebar.multiselect(label="Variables to plot", options=numeric_cols)
print(feature_selection)
df_features = data[feature_selection]

plotly_figure = px.line(data_frame=df_features,
                        x=df_features.index, y=feature_selection,
                        title=(str("test") + ' ' +'timeline')
                        )

st.plotly_chart(plotly_figure)



