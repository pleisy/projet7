import streamlit as st
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import plotly.figure_factory as p_ff
import time

st.write("Version de STREAMLIT: " + st.__version__)

st.title('Dashboard Scoring Credit')


# Chargement des données
@st.cache
def load_data(f_csv, nrows=None):
    data = pd.read_csv(f_csv, nrows=nrows)
    return data

data_load_state = st.text('Loading data...')
data1 = load_data('./resultats/sample_submission_100000.csv')
x = st.slider("Nombre de clients recherchés: ", min_value=100,   
                       max_value=data1.shape[0], value=data1.shape[0]//10, step=data1.shape[0]//100)
#add_slider = st.sidebar.slider('Nombre de données',0.0, 10000.0, 2000)

data2 = load_data('./resultats/feature_importance_model_A.csv', nrows=x)
data_load_state.text("Done! (using st.cache)")


# Selectbox to the sidebar:
add_selectbox = st.sidebar.selectbox(
    'Navigation',
    ('Home', 'Client', 'Conseiller', 'Help')
)
if add_selectbox == 'Client':
    st.write('Page Client')
elif add_selectbox == 'Conseiller':
    st.write('Page Conseiller')
elif add_selectbox == 'Home':
    st.write('Retour à la Home page')
else:
    st.write('you have selected help page')


# Supprime le message d'erreur
# st.set_option('deprecation.showPyplotGlobalUse', False)
st.subheader('Histogramme des pronostics')
df = pd.DataFrame(data1[:x], columns=['TARGET'])
fig, ax = plt.subplots(figsize=(5,2))
ax.hist(df['TARGET'], bins=150)
plt.show()
st.pyplot(fig)


# Montre des données
if st.checkbox("Show/hide data"):
    st.subheader('Raw data')
    st.write(data2[:5])

    hist_values2 = np.histogram(data2['gain'], bins=50)  # st.bar_chart(data2['gain'])
    hist_values3 = np.histogram(data2['split'], bins=25)
    #st.bar_chart(hist_values3)
    st.subheader('Histogramme des gains')
    fig, ax = plt.subplots()
    ax.hist(hist_values2, bins=20)
    st.pyplot(fig)
# Exemple d'une Progress bar
    my_bar = st.progress(0)
    for percent_complete in range(100):
        time.sleep(0.01)
        my_bar.progress(percent_complete + 1)


# Interface client / conseiller
status = st.radio("What is your status",("Client", "Conseiller"))
if status == 'Client':
    st.success("Interface Client ...")
if status == 'Conseiller':
    st.success("Interface Conseiller ...")


col_client, col_conseil = st.beta_columns(2)    # You can use a column just like st.sidebar

# Client
col_client.button('Interface Client')
col_conseil.button('Interface Conseiller')

with col_client:
    chosen = st.radio(
        'Sorting hat',
        ("Gryffindor", "Ravenclaw", "Hufflepuff", "Slytherin"))
    st.write(f"You are in {chosen} house!")
